export class Card{
    title: string;
    rung: string;
    isTrum?: boolean = false;
    selected?: boolean = false;
    thrown?: boolean = false;
}