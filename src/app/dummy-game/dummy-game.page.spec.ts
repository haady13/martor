import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyGamePage } from './dummy-game.page';

describe('DummyGamePage', () => {
  let component: DummyGamePage;
  let fixture: ComponentFixture<DummyGamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyGamePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyGamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
