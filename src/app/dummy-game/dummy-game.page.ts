import { Component, OnInit } from '@angular/core';
import { Card } from '../Card';

@Component({
  selector: 'app-dummy-game',
  templateUrl: './dummy-game.page.html',
  styleUrls: ['./dummy-game.page.scss'],
})
export class DummyGamePage implements OnInit {
  public card: Card[];
  public currentHand: number = 0;
  constructor() { }

  ngOnInit() {
    this.card = [
      {
        title: 'A',
        rung: 'Heart',
        isTrum: true
      },
      {
        title: 'A',
        isTrum: true,
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart',
        isTrum : true
      },
    ]
  }

  swipe(ev, item: Card) {
    if (ev.deltaY < 0) {
      console.log(ev);
      let value = -1 * ev.deltaY;
      if (value > 30) {
        item.selected = true;
        item.thrown = true;
        this.throwCard(item);
      }
    }
  }

  throwCard(item: Card) {
      console.log("in");
      item.thrown = true;
  }

  click(elem: Card,id: string) {
    if (!elem.selected) {
      for (let item of this.card) {
        if (item != elem) {
          item.selected = false;
        } else {
          item.selected = true;
        }
      }
    } else {
      this.throwCard(elem);
    }
  }

}
