import { Component, OnInit } from '@angular/core';
import { Card } from '../Card';

@Component({
  selector: 'app-game-screen',
  templateUrl: './game-screen.page.html',
  styleUrls: ['./game-screen.page.scss'],
})
export class GameScreenPage implements OnInit {
  public card: Card[];
  public currentHand: number = 0;
  constructor() { }

  ngOnInit() {
    this.card = [
      {
        title: 'A',
        rung: 'Heart',
        isTrum: true
      },
      {
        title: 'A',
        isTrum: true,
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
      {
        title: 'A',
        rung: 'Heart'
      },
    ]
  }

  swipe(ev, item: Card,id: string) {
    if (ev.deltaY < 0) {
      console.log(ev);
      let value = -1 * ev.deltaY;
      if (value > 30) {
        item.selected = true;
        item.thrown = true;
        this.throwCard(id);
      }
    }
  }

  throwCard(id: string, replaceId: string = 'cardOne') {
      console.log("in");
      var elem = document.getElementById('' + replaceId + '');
      var card = document.getElementById('' + id + '');
  }

  click(elem: Card,id: string) {
    let state  : boolean = false;
    if (!elem.selected) {
      for (let item of this.card) {
        if (item != elem) {
          item.selected = false;
        } else {
          item.selected = true;
        }
      }
    } else {
      this.throwCard(id);
    }
  }

}
