import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  constructor() {}

  ngOnInit(){
    this.changeCardSize();
  }

  changeCardSize(){
    let myCardsHeight = document.getElementById('myCard').offsetHeight;
    let myCardsWidth = document.getElementById('myCard').offsetHeight;
    let otherCardHeight = document.getElementById('otherCard').offsetHeight ; 
    let otherCardWidth = document.getElementById('otherCard').offsetWidth ; 
    console.log(myCardsHeight,myCardsWidth,otherCardHeight,otherCardWidth);
  }

}
